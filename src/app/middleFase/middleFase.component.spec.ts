import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleFaseComponent } from './middleFase.component';

describe('MiddleFaseComponent', () => {
  let component: MiddleFaseComponent;
  let fixture: ComponentFixture<MiddleFaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleFaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
