import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-middle',
  templateUrl: './middleFase.component.html',
  styleUrls: ['./middleFase.component.css']
})
export class MiddleFaseComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  @Input()
  acao1: { id: number, comentario: string };

  @Input()
  acao2: { id: number, comentario: string };

  @Input()
  acao3: { id: number, comentario: string };

  @Input()
  acao4: { id: number, comentario: string };

  @Input()
  comentario: string;

}
