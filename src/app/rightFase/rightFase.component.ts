import { Component, OnInit, Input, Directive } from '@angular/core';

@Component({
  selector: 'app-right',
  templateUrl: './rightFase.component.html',
  styleUrls: ['./rightFase.component.css']
})
export class RightFaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }

  @Input()
  fase: string;

  @Input()
  valor: string;

  @Input()
  pontuacao: string;
}
