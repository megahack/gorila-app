import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'start', loadChildren: () => import('./game/start/start.module').then(m => m.StartModule) },
  { path: 'fase/1', loadChildren: () => import('./game/fase/fase.module').then(m => m.FaseModule) },
  { path: 'fase/2', loadChildren: () => import('./game/fase2/fase.module').then(m => m.FaseModule) },
  { path: 'final', loadChildren: () => import('./game/final/final.module').then(m => m.FinalModule) },
  { path: '', redirectTo: 'start', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
