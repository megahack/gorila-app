import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fase',
  templateUrl: './fase.component.html',
  styleUrls: ['./fase.component.css']
})
export class FaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title: string = 'Fase 2';
  description: string = 'Você comprou 50 ações por R$10,00, hoje, depois de 10 dias está valendo R$5,00.';
  nome: string = 'Matheus';
  valor: string = '500,00';
  pontuacao: string = '30';
  comentario: string = 'Qual ação tomar?'

  acao1 = { id: 1, comentario: 'Vender tudo.' };
  acao2 = { id: 2, comentario: 'Comprar mais.' };
  acao3 = { id: 3, comentario: 'Manter.' };
  acao4 = { id: 4, comentario: 'Vender uma parte.' };

  fase: string = '2'
}
