import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaseComponent } from './fase.component';
import { FaseRoutingModule } from './fase-routing.module';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { HeaderModule } from '../../header/header.module';
import { LeftFaseModule } from '../../leftFase/leftFase.module';
import { MiddleFaseModule } from '../../middleFase/middleFase.module';
import { RightFase2Module } from '../../rightFase2/rightFase.module';

@NgModule({
  declarations: [FaseComponent],
  imports: [
    CommonModule,
    FaseRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    HeaderModule,
    LeftFaseModule,
    MatGridListModule,
    RightFase2Module,
    MiddleFaseModule
  ]
})
export class FaseModule { }
