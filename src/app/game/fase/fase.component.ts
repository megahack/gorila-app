import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fase',
  templateUrl: './fase.component.html',
  styleUrls: ['./fase.component.css']
})
export class FaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title: string = 'Fase 1';
  description: string = 'Por qual ativo se interessa?';
  nome: string = 'Matheus';
  valor: string = '1000,00';
  pontuacao: string = '0';
  comentario: string = 'Qual ação tomar?';
  acao1 = { id: 1, comentario: 'Ações' };
  acao2 = { id: 2, comentario: 'FII' };
  acao3 = { id: 3, comentario: 'CDB' };
  acao4 = { id: 4, comentario: 'Tesouro' };

  fase: string = '1'
}
