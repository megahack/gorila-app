import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartComponent } from './start.component';
import { StartRoutingModule } from './start-routing.module';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { StartFormModule } from './start-form/start-form.module';
import { HeaderModule } from './../../header/header.module';

@NgModule({
  declarations: [StartComponent],
  imports: [
    CommonModule,
    StartRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    StartFormModule,
    HeaderModule
  ]
})
export class StartModule { }
