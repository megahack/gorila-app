import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title: string = 'Bem vindo ao mundo dos investimentos!';
  description: string = 'Agora precisamos que você mergulhe em alguns desafios para conseguirmos te ajudar daqui pra frente. Vamos nessa?';

}
