import { JogadorService } from './../../../../servers/jogador.service';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-form',
  templateUrl: './start-form.component.html',
  styleUrls: ['./start-form.component.css']
})
export class StartFormComponent implements OnInit {

  constructor(
    private jogadorService: JogadorService,
    private router: Router) {

  }

  ngOnInit(): void {
  }

  nome = new FormControl();
  idade = new FormControl();
  sexo: 'MASCULINO' | 'FEMININO';
  email = new FormControl();
  backColorMasculino: string;
  backColorFeminino: string;

  createJogador() {
    this.jogadorService.create(
      this.nome.value,
      this.email.value,
      this.idade.value,
      this.sexo)
      .subscribe(
        data => {
          this.router.navigateByUrl('/fase/1');
        },
        error => {
          console.error(error);
        }
      );
  }

  setSexo(sexo: 'MASCULINO' | 'FEMININO') {
    this.sexo = sexo;

    if (this.sexo === 'MASCULINO') {
      this.backColorMasculino = '#f2f2f2';
      this.backColorFeminino = '#FFF';
    }
    else {
      this.backColorMasculino = '#FFF';
      this.backColorFeminino = '#f2f2f2';
    }
  }

}
