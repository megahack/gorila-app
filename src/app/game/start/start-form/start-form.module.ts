import { HttpClientModule } from '@angular/common/http';
import { JogadorService } from './../../../../servers/jogador.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartFormComponent } from './start-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';



@NgModule({
  declarations: [StartFormComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonToggleModule
  ],
  exports: [
    StartFormComponent
  ],
  providers: [
    JogadorService
  ]
})
export class StartFormModule { }
