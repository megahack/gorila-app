import { Component, OnInit } from '@angular/core';
import { CalcularPerfilService } from 'src/servers/calcular-perfil.service';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  constructor(private serviceCalcularPerfil: CalcularPerfilService) { }

  ngOnInit(): void {
    // this.calcularPerfil();
  }

  title: string = 'Parabéns, você chegou até o fim!';
  description: string = 'Estamos felizes por você ter completado todo o nosso desafio';
  perfil: string = "ARROJADO";
  pontos: number = 30;
  nomeJogador: string = 'Matheus';
  descriptionPerfil: string = ' tem como objetivo a maior rentabilidade possível, contudo você está disposto a assumir maiores' +
    'riscos.Você conhece os produtos de investimento e suporta as oscilações de mercado, sem se preocupar muito.';


  respostaJogador = {
    acoes: [
      {
        id: 4
      },
      {
        id: 7
      }
    ],
    jogador: {
      id: 2
    },
    jogo: {
      id: 2
    }
  }

  calcularPerfil() {
    this.serviceCalcularPerfil.resultadoJogos(this.respostaJogador).subscribe(
      data => {
        this.perfil = data.perfil;
        this.pontos = data.valorTotalCalculado;
        this.nomeJogador = data.jogador.nome;
      },
      error => {
        console.error(error);
      }
    );
  }
}
