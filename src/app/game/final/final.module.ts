import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinalComponent } from './final.component';

import { FinalRoutingModule } from './final-routing.module';
import { HeaderModule } from './../../header/header.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { CalcularPerfilService } from 'src/servers/calcular-perfil.service';


@NgModule({
  declarations: [FinalComponent],
  imports: [
    CommonModule,
    FinalRoutingModule,
    HeaderModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [
    CalcularPerfilService
  ]
})
export class FinalModule { }
