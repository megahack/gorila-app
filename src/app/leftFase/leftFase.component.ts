import { Component, OnInit, Input, Directive } from '@angular/core';

@Component({
  selector: 'app-left',
  templateUrl: './leftFase.component.html',
  styleUrls: ['./leftFase.component.css']
})
export class LeftFaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }

  @Input()
  nome: string;

  @Input()
  valor: string;

  @Input()
  pontuacao: string;
}
