import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftFaseComponent } from './leftFase.component';

describe('LeftFaseComponent', () => {
  let component: LeftFaseComponent;
  let fixture: ComponentFixture<LeftFaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftFaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
