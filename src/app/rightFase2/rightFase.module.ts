import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RightFase2Component } from './rightFase.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [RightFase2Component],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule
  ],
  exports: [
    RightFase2Component
  ]
})
export class RightFase2Module { }
