import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightFaseComponent } from './rightFase.component';

describe('RightFaseComponent', () => {
  let component: RightFaseComponent;
  let fixture: ComponentFixture<RightFaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightFaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
