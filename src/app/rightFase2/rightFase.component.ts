import { Component, OnInit, Input, Directive } from '@angular/core';

@Component({
  selector: 'app-right2',
  templateUrl: './rightFase.component.html',
  styleUrls: ['./rightFase.component.css']
})
export class RightFase2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }

  @Input()
  fase: string;

  @Input()
  valor: string;

  @Input()
  pontuacao: string;
}
