import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from './../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JogadorService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  create(nome: string, email: string, idade: number, sexo: string): Observable<any> {
    return this.http.post(
      environment.servidor + environment.pathJogador,
      {
        email: email,
        senha: 123,
        idade: idade,
        nome: nome,
        sexo: sexo
      },
      this.httpOptions)
      .pipe(
        tap(
          data => data,
          error => error)
      );
  }
}
