import { TestBed } from '@angular/core/testing';

import { CalcularPerfilService } from './calcular-perfil.service';

describe('CalcularPerfilService', () => {
  let service: CalcularPerfilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalcularPerfilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
