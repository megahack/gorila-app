import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from './../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalcularPerfilService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  resultadoJogos(respostasJogador: {}): Observable<any> {
    return this.http.post(
      environment.servidor + environment.pathCalcularPerfil,
      respostasJogador,
      this.httpOptions)
      .pipe(
        tap(
          data => data,
          error => error)
      );
  }
}
